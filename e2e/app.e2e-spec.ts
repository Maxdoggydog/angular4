import { NewscrushPage } from './app.po';

describe('newscrush App', () => {
  let page: NewscrushPage;

  beforeEach(() => {
    page = new NewscrushPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});

# Try to make work Compass
# It is always looking for sass files

# Set this to the root of your project when deployed:
http_path = "/"

# Just like we need
css_dir = "../"
sass_dir = "./settings-scss"
images_dir = "./media"
environment     = :development
relative_assets = true

# You can select your preferred output style here (can be overridden via the command line):
output_style = :expanded

# When you are ready to launch your WP theme comment out (3) and uncomment the line below
# output_style = :compressed

# To disable debugging comments that display the original location of your selectors. Uncomment:
# line_comments = false

# Like that is better
preferred_syntax = :scss


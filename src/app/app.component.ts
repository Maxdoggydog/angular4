import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Http, Response } from '@angular/http';
import { HttpErrorResponse } from '@angular/common/http';
import { NotificationService } from 'ng2-notify-popup';
import { NamescapeService } from './services/namescape.service';
import { LocalStorageService } from 'ng2-webstorage';

import { environment } from '../environments/environment';

@Component({
  templateUrl: './app.component.html',
})

export class AppComponent implements OnInit {
  private headers: Headers = new Headers();
  private isConnected : Boolean;
  private newsfeed : String;
  constructor(
    private http: Http,
    private namescapeService: NamescapeService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private localSt: LocalStorageService,
    private notify: NotificationService
  ) {

  }

  ngOnInit(): void {
    this.initialiseApp();
  }

  /**
   * Initialise Login or Register
   */
  initialiseApp () {
    let apiUrl: string = environment.domain + 'api/user';
    return this.http.post(apiUrl, {'token' : this.localSt.retrieve('token')})
    .subscribe(
      res => {
        // Success
        let user = res.json().result;
        if(!user) return this.router.navigate(['/login_or_register']);

        let newsfeed_id : Number = parseInt(this.activatedRoute.snapshot.params['id']);
        let apiUrl: string = environment.domain + 'api/newsfeedbelongstouser';
        this.http.post(apiUrl, {'token' : this.localSt.retrieve('token'), 'newsfeed': newsfeed_id})
        .map((res : Response) => res.json())
        .subscribe(
          data => {
            // Redirect to the first newsfeed if connected
            this.router.navigate(['/newscrush/', user.newsfeed_ids[0]]);
          }
        );
      }
    );
  }
}

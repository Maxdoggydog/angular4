import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule, Routes } from '@angular/router';
import { Ng2Webstorage } from 'ng2-webstorage';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { TagInputModule } from 'ngx-chips';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MomentModule } from 'angular2-moment';
import { TruncateModule } from 'ng2-truncate';
import 'rxjs/add/operator/debounceTime';

import 'web-animations-js';

// Import library
import { NgNotifyPopup } from 'ng2-notify-popup';
import { NotificationService } from 'ng2-notify-popup';


/* Pages */
import { AppComponent } from './app.component';
import { LoginOrRegisterComponent } from './login/login_or_register.component';
import { LoginComponent } from './login/login.component';
import { RegisterUsernameComponent } from './login/register_username.component';
import { RegisterPasswordComponent } from './login/register_password.component';
import { RegisterPictureComponent } from './login/register_picture.component';
import { MainComponent } from './main/main.component';
import { PanelActivitiesComponent } from './main/panel/panel_activities.component';
import { PanelTagsComponent } from './main/panel/panel_tags.component';
import { PanelTeammatesComponent } from './main/panel/panel_teammates.component';
import { HeaderComponent } from './main/header/header.component';
import { LinkComponent } from './link.component';
import { MainUrlComponent } from './main/main_part/main_url.component';
import { MainDashboardComponent } from './main/main_part/main_dashboard.component';
import { MainNewsComponent } from './main/main_part/main_news.component';
import { ReaderComponent } from './main/main_part/reader/reader.component';

/* Services */
import { NamescapeService } from './services/namescape.service';
import { UserService } from './services/user.service';
import { GamificationService } from './services/gamification.service';
import { NewsService } from './services/news.service';

TagInputModule.withDefaults({
    tagInput: {
        placeholder: 'Add new tag'
    }
});

const appRoutes: Routes = [
  {path: '', component: LinkComponent, children: [
    { path: '', component: AppComponent, children: [
      { path: 'login_or_register', component: LoginOrRegisterComponent },
      { path: 'login', component: LoginComponent },
      { path: 'register_username', component: RegisterUsernameComponent },
      { path: 'register_password', component: RegisterPasswordComponent },
      { path: 'register_picture', component: RegisterPictureComponent }
    ] },
    { path: 'newscrush/:id', component: MainComponent },
    { path: '', pathMatch: 'full', redirectTo: 'login_or_register' }
  ]}
];

@NgModule({
  declarations: [
    LinkComponent, // ==> This module help for the templating and for the layout
    AppComponent,
    LoginOrRegisterComponent,
    LoginComponent,
    RegisterUsernameComponent,
    RegisterPasswordComponent,
    RegisterPictureComponent,
    MainComponent,
    PanelActivitiesComponent,
    PanelTagsComponent,
    PanelTeammatesComponent,
    HeaderComponent,
    MainUrlComponent,
    MainDashboardComponent,
    MainNewsComponent,
    ReaderComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: false }
    ),
    NgNotifyPopup,
    Ng2Webstorage,
    TagInputModule,
    BrowserAnimationsModule,
    MomentModule,
    TruncateModule,
  ],
  providers: [NamescapeService, UserService, GamificationService, NotificationService, NewsService],
  bootstrap: [
    LinkComponent
  ]
})

export class AppModule {

}

platformBrowserDynamic().bootstrapModule(AppModule);
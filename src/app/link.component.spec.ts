import { TestBed, async } from '@angular/core/testing';
import { RouterModule, Router } from '@angular/router';
import { LinkComponent } from './link.component';
import { RouterTestingModule } from '@angular/router/testing';


describe('LinkComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        LinkComponent
      ],
      imports: [
        RouterTestingModule
      ],
      providers: [
      ]
    }).compileComponents();
  }));

  it('should create the app', async(() => {
    const fixture = TestBed.createComponent(LinkComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));

  it('router-outlet exist in link', async(() => {
    const fixture = TestBed.createComponent(LinkComponent);
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('router-outlet')).toBeTruthy(); // If that element exist in DOM
  }));
});

import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Http, Response, Headers, RequestOptions, RequestMethod } from '@angular/http';
import { HttpErrorResponse } from '@angular/common/http';
import { NotificationService } from 'ng2-notify-popup';
import { DOCUMENT } from '@angular/platform-browser';
import { NamescapeService } from './services/namescape.service';
import { FrontService } from './services/front.service';
import { LocalStorageService } from 'ng2-webstorage';

import { environment } from '../environments/environment';

@Component({
  selector: 'app-root',
  templateUrl: './link.component.html',
  providers: [NotificationService, FrontService]
})

export class LinkComponent implements OnInit {
  constructor(
    private http: Http,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private notify: NotificationService,
    private namescapeService: NamescapeService,
    private frontService: FrontService,
    private localSt: LocalStorageService
  )
  {

  }

  ngOnInit(): void {
    // If user not connected
    // Back to login
    this.CheckConnected();
  }

  /**
   * Check if the use is connected
   */
  CheckConnected() {
    if(!this.localSt.retrieve('token')) return this.router.navigate(['/login_or_register']);
    let apiUrl: string = environment.domain + 'api/user';
    return this.http.post(apiUrl, {'token' : this.localSt.retrieve('token')})
    .subscribe(
      response => {
        // Return the value from the server
        console.log(response);
      },
      (err: HttpErrorResponse) => {
        console.log("err", err);
        if(err.status == 400){
          this.notify.show("You need to login", { position:'top', duration:'2000', type: 'error', sticky: true });
        }
        if(err.status == 401){
          this.notify.show("Your session had expired", { position:'top', duration:'2000', type: 'error', sticky: true });
        }
        // Anyway if error
        // Go back to login
        this.router.navigate(['/login_or_register']);
      }
    )
  }

  /**
   * Remove the blur effect
   */
  removeBlurEffect(){
    this.frontService.removeBlur();
  }
}

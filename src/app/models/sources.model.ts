import { Injectable } from '@angular/core';
import { NamescapeService } from '../services/namescape.service';

@Injectable()
export class Source {
  public id: number;
  public logo: string;
  public name: string;
  constructor(){

  }

  sourceProfile (objNews) : Source {
    this.id = objNews.id;
    this.logo = objNews.logo;
    this.name = objNews.name;
    return this;
  }
}

import { Injectable } from '@angular/core';
import { NamescapeService } from '../services/namescape.service';
import { Source } from './sources.model';

@Injectable()
export class News {
  public id: number;
  public avg_rate: number;
  public comment: Array<any>;
  public comments_count: number;
  public description: string;
  public created_at: Date;
  public updated_at: Date;
  public highlighted_text: string;
  public votes : Array<any>;
  public views : Array<any>;
  public url : string;
  public tags : Array<any>;
  public tags_count : number;
  public newsfeed_ids : Array<any>;
  public team_ids : Array<any>;
  public source : Source;

  constructor(){

  }

  newsProfile (objNews) : News {
    this.id = objNews.id;
    this.avg_rate = objNews.avg_rate;
    this.comment = objNews.comment;
    this.comments_count = objNews.comments_count;
    this.description = objNews.description;
    this.created_at = objNews.created_at;
    this.updated_at = objNews.updated_at;
    this.highlighted_text = objNews.highlighted_text;
    this.votes = objNews.votes;
    this.views = objNews.views;
    this.url = objNews.url;
    this.tags = objNews.tags;
    this.tags_count = objNews.tags_count;
    this.newsfeed_ids = objNews.newsfeed_ids;
    this.team_ids = objNews.newsfeed_ids;
    this.source = objNews.source;
    return this;
  }
}

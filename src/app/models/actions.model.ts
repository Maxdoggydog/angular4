import { Injectable } from '@angular/core';
import { NamescapeService } from '../services/namescape.service';

@Injectable()
export class Action {
  id: number;
  user_id: number;
  badges: object;
  created_at: Date;
  updated_at: Date;

  constructor(){

  }

  actionProfile (objAction) : Action {
    this.id = objAction.id;
    this.user_id = objAction.user_id;
    this.badges = objAction.badges;
    this.created_at = objAction.created_at;
    this.updated_at = objAction.updated_at;
    return this;
  }
}

import { Injectable } from '@angular/core';
import { NamescapeService } from '../services/namescape.service';
import { Action } from './actions.model';

@Injectable()
export class User {
  public id: number;
  public username: string;
  public firstname: string;
  public lastname: string;
  public email: string;
  public created_at: Date;
  public updated_at: Date;
  public avatar: string;
  public gamification: Action;
  public newsfeed_ids : Array<any>;
  public team_ids : Array<any>;
  public team_private : string;

  constructor(){

  }

  userProfile (objUser) : User {
    this.id = objUser.id;
    this.username = objUser.username;
    this.firstname = objUser.firstname;
    this.lastname = objUser.lastname;
    this.email = objUser.email;
    this.created_at = objUser.created_at;
    this.updated_at = objUser.updated_at;
    this.avatar = objUser.avatar;
    this.gamification = objUser.gamification;
    this.newsfeed_ids = objUser.newsfeed_ids;
    this.team_ids = objUser.team_ids;
    this.team_private = objUser.team_private;
    return this;
  }
}

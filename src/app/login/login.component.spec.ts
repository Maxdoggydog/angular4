import { TestBed, async } from '@angular/core/testing';
import { RouterModule, Router } from '@angular/router';
import { LoginComponent } from './login.component';
import { LoginOrRegisterComponent } from './login_or_register.component';
import { LinkComponent } from './../link.component';
import { AppComponent } from './../app.component';
import { RouterTestingModule } from '@angular/router/testing';
import { NotificationService } from 'ng2-notify-popup';
import { Http, HttpModule, BaseRequestOptions } from '@angular/http';
import { MockBackend, MockConnection } from '@angular/http/testing';
import { NgNotifyPopup } from 'ng2-notify-popup';
import { FormsModule } from '@angular/forms';
import { NamescapeService } from './../services/namescape.service';
import { LocalStorageService } from 'ng2-webstorage';

declare var jquery:any;   // not required
declare var $ :any;

describe('LoginComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        LinkComponent,
        AppComponent,
        LoginOrRegisterComponent,
        LoginComponent,
      ],
      imports: [
        RouterTestingModule.withRoutes([{path: 'login_or_register', component: LoginOrRegisterComponent}]),
        NgNotifyPopup,
        FormsModule
      ],
      providers: [
        {
          provide: Http, useFactory: (backend, options) => {
            return new Http(backend, options);
          },
          deps: [MockBackend, BaseRequestOptions]
        },
        NamescapeService,
        NotificationService,
        MockBackend,
        BaseRequestOptions,
        LocalStorageService,
      ]
    }).compileComponents();
  }));

  it('should create the app LoginComponent', async(() => {
    const fixture = TestBed.createComponent(LoginComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));

  it('input#email exist', async(() => {
    const fixture = TestBed.createComponent(LoginComponent);
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    const email = compiled.querySelector('#email');
    const password = compiled.querySelector('#password');
    const button = compiled.querySelector('button');

    fixture.detectChanges();

  }));
});

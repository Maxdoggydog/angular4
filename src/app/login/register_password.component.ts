import { Component } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { NotificationService } from 'ng2-notify-popup';
import { NamescapeService } from '../services/namescape.service';
import { LocalStorageService } from 'ng2-webstorage';

declare var jquery:any;   // not required
declare var $ :any;

@Component({
  templateUrl: './register_password.component.html',
  providers: [NotificationService]
})

export class RegisterPasswordComponent {
  private isSame : Boolean;
  data: any = {};
  constructor(
    private router: Router,
    private namescapeService: NamescapeService,
    private notify: NotificationService,
    private localSt: LocalStorageService
  ) {
    if(!this.localSt.retrieve('username') || !this.localSt.retrieve('firstname') || !this.localSt.retrieve('lastname') || !this.localSt.retrieve('email')){
      this.router.navigate(['/register_username']);
    }
  }

  samePassword() : void {
    if($("#password").val() !== $("#password_confirmation").val()){
      this.isSame = true;
    }else{
      this.isSame = false;
    }
    console.log(this.isSame);
  }

  signUp2(f: NgForm) : void {
    if(this.isSame)
      this.notify.show("Not the same password", { position:'top', duration:'2000', type: 'error', sticky: true });

    if(f.valid && !this.isSame){
      this.localSt.store('password', f.value.password);
      this.localSt.store('password_confirmation', f.value.password);
      this.router.navigate(['/register_picture']);
    }
  }
}

import { Component } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { Http, Response, Headers, RequestOptions, RequestMethod } from '@angular/http';
import { HttpErrorResponse } from '@angular/common/http';
import { NotificationService } from 'ng2-notify-popup';
import { DOCUMENT } from '@angular/platform-browser';
import { NamescapeService } from '../services/namescape.service';
import { LocalStorageService } from 'ng2-webstorage';
// Allow me to use jQuery
declare var jquery:any;   // not required
declare var $ :any;

import { environment } from '../../environments/environment';

@Component({
  templateUrl: './login.component.html',
  providers: [NotificationService]
})

export class LoginComponent {
  private headers: Headers = new Headers();
  private newsfeed : Number;
  private user;

  data: any = {};
  constructor(
    private http: Http,
    private router: Router,
    private notify: NotificationService,
    private namescapeService: NamescapeService,
    private localSt: LocalStorageService
  ) {
    localStorage.clear();
  }

  login(f: NgForm) {
    let apiUrl: string;
    apiUrl = environment.domain + 'api/login';
    if(f.valid){
      this.data = f.value;

      return this.http.post(apiUrl, this.data)
      .map((res : Response) => res.json())
      .subscribe(
        response => {
          // Return the value from the server
          console.log(response);
          // Je Crée la class user, et je l'intègre avec la réponse et je le retourne dans le main page
          let token = response.token;
          this.localSt.store('token', token);
          this.newsfeed = response.newsfeed;
          this.user = response.user;
          this.localSt.store('user', this.user);
          this.router.navigate(['/newscrush/', this.newsfeed]);
        },
        // Errors will call this callback instead:
        (err: HttpErrorResponse) => {
          if (err.error instanceof Error) {
            // A client-side or network error occurred. Handle it accordingly.
            console.log('An error occurred:', err.error.message);
            this.notify.show("Problem occurred, try later", { position:'top', duration:'2000', type: 'error', sticky: true });
          } else {
            // The backend returned an unsuccessful response code.
            // The response body may contain clues as to what went wrong,
            console.log(`Backend returned code ${err.status}, body was: ${err.statusText}`);
            this.notify.show("Problem occurred on the server, try again or contact the administrator", { position:'top', duration:'2000', type: 'error', sticky: true });
          }
        }
      );
    }
  }

}

import { Component } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { Http, Response, Headers, RequestOptions, RequestMethod } from '@angular/http';
import { HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { NotificationService } from 'ng2-notify-popup';
import { NamescapeService } from '../services/namescape.service';
import { LocalStorageService } from 'ng2-webstorage';

declare var jquery:any;
declare var $ :any;

import { environment } from '../../environments/environment';

@Component({
  selector: 'div.loginOrRegister',
  templateUrl: './register_username.component.html',
  providers: [NotificationService]
})

export class RegisterUsernameComponent {
  private headers: Headers = new Headers();
  private isValidUsername : boolean;
  private isValidEmail : boolean;
  data: any = {};

  constructor(
    private http: Http,
    private router: Router,
    private notify: NotificationService,
    private namescapeService: NamescapeService,
    private localSt: LocalStorageService
  ) {
    // If localStorage exist ==> delete them to start again from the begening
    localStorage.clear();
    this.isValidUsername = false;
    this.isValidEmail = false;
  }

  /**
   * Check if the username already exist
   */
  checkusername() {
    let username = $('#username').val();
    return this.http.post(environment.domain+'api/checkusername', {'username': username})
      .subscribe(
        response => {
          console.log(response);
          if(response.statusText == 'OK'){
            if(response.json()['error'] == true){
              this.isValidUsername = false;
              this.notify.show('This username already exist', { position:'top', duration:'2000', type: 'error', sticky: true });
            }else{
              this.isValidUsername = true;
            }
          }
        }
      )
  }

  /**
   * Check if the email already exist
   */
  checkmail() {
    let email = $('#email').val();
    return this.http.post(environment.domain+'api/checkemail', {'email': email})
      .subscribe(
        response => {
          console.log(response);
          if(response.statusText == 'OK'){
            if(response.json()['error'] == true){
              this.isValidEmail = false;
              this.notify.show('This email already exist', { position:'top', duration:'2000', type: 'error', sticky: true });
            }else{
              this.isValidEmail = true;
            }
          }
        }
      )
  }

  signUp1(f: NgForm) {
    let apiUrl: string;
    apiUrl = environment.domain+'api/test/post';
    if(!this.isValidEmail){
      this.notify.show('Change the email please', { position:'top', duration:'2000', type: 'error', sticky: true });
    }else if(!this.isValidUsername){
      this.notify.show('Change the username please', { position:'top', duration:'2000', type: 'error', sticky: true });
    }

    if(f.valid && this.isValidEmail && this.isValidUsername){
      this.data = f.value;

      this.headers.append('Access-Control-Allow-Origin', '*');
      this.headers.append('Content-Type', 'application/json');
      this.headers.append('X-CSRF-TOKEN', $('meta[name="csrf-token"]').attr('content'));
      let options = new RequestOptions({ headers: this.headers,method: RequestMethod.Post, url: apiUrl});

      return this.http.post(apiUrl, this.data, options)
      .subscribe(
        response => {
          if(response.statusText == 'OK'){
            this.localSt.store('username', response.json().username);
            this.localSt.store('firstname', response.json().firstname);
            this.localSt.store('lastname', response.json().lastname);
            this.localSt.store('email', response.json().email);
            this.router.navigate(['/register_password']);
          }
        },
        (err: HttpErrorResponse) => {
          if (err.error instanceof Error) {
            console.log('An error occurred:', err.error.message);
            this.notify.show("Problem occurred, try later", { position:'top', duration:'2000', type: 'error', sticky: true });
          } else {
            console.log(`Backend returned code ${err.status}, body was: ${err.statusText}`);
            this.notify.show("Problem occurred on the server, try again or contact the administrator", { position:'top', duration:'2000', type: 'error', sticky: true });
          }
        }
      );
    }
  }
}

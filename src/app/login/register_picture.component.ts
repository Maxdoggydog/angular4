import { Component } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { Http, Response, Headers, RequestOptions, RequestMethod } from '@angular/http';
import { HttpErrorResponse } from '@angular/common/http';
import { NotificationService } from 'ng2-notify-popup';
import { DOCUMENT } from '@angular/platform-browser';
import { NamescapeService } from '../services/namescape.service';
import { LocalStorageService, SessionStorageService, LocalStorage, SessionStorage } from 'ng2-webstorage';
// Allow me to use jQuery
declare var jquery:any;   // not required
declare var $ :any;

import { environment } from '../../environments/environment';

@Component({
  templateUrl: './register_picture.component.html',
  providers: [NotificationService]
})

export class RegisterPictureComponent {
  private headers: Headers = new Headers();
  // Allow me to know if I waiting for something
  private isWaiting : Boolean;
  private newsfeed : Number;
  private pictureName : String;
  private pathPicture : String;
  constructor(
    private http: Http,
    private router: Router,
    private notify: NotificationService,
    private namescapeService: NamescapeService,
    private localSt:LocalStorageService
  ){
    if(!this.localSt.retrieve('password')){
      this.router.navigate(['/register_password']);
      this.isWaiting = false;
    }
  }

  /**
   * Upload file 'Picture'
   */
  fileUpload(event) {
    let urlPicture : string = environment.domain + 'api/uploadpicture';
    let fileList: FileList = event.target.files;
    if(fileList.length > 0) {
      let file: File = fileList[0];
      console.log("file ==> ", file);
      let formData = new FormData();
      formData.append('photo', file, file.name);

      let headers = new Headers();
      headers.append('Accept', 'application/json');

      let options = new RequestOptions({ headers: headers });
      this.http.post(urlPicture, formData, options)
      .map(res => res.json())
      // .catch(error => Observable.throw(error))
      .subscribe(
        data => {
          this.localSt.store('path', data.path);
          this.pathPicture = environment.domain + data.path;
        },
        error => console.log(error)
      )
    }
  }

  /**
   * Trigger the btn to get the picture
   */
  picture(): void {
    $('body .nc-login-box form #pdffile').click();
  }

  /**
   * Get the name on the field
   */
  profilePicture(event) : void {
    this.pictureName = $('body .nc-login-box form #pdffile').val().split('\\')[2];
    this.fileUpload(event);
  }

  /**
   * If form valid ==> create user after
   */
  signUp3(f: NgForm) : void {
    if(f.valid){
      console.log("value f",f)
      console.log("value form",f.value)
      localStorage.setItem('picture', f.value.file);
      this.createUser();
    }else{
      this.notify.show("Don't forget you're avatar", { position:'top', duration:'2000', type: 'error', sticky: true });
    }
  }

  /**
   * Going to create the user here
   */
  createUser() {
    let apiUrl: string;
    apiUrl = environment.domain + 'api/createuser';
    this.isWaiting = true;
    let value = {
      username: this.localSt.retrieve('username'),
      firstname: this.localSt.retrieve('firstname'),
      lastname: this.localSt.retrieve('lastname'),
      email: this.localSt.retrieve('email'),
      password: this.localSt.retrieve('password'),
      password_confirmation: this.localSt.retrieve('password_confirmation'),
      avatar: this.localSt.retrieve('path'),
    };

    this.headers.append('Access-Control-Allow-Origin', '*');
    // this.headers.append('processData', false);
    this.headers.append('Content-Type', 'application/json');
    this.headers.append('X-CSRF-TOKEN', $('meta[name="csrf-token"]').attr('content'));
    let options = new RequestOptions({ headers: this.headers,method: RequestMethod.Post, url: apiUrl});

    return this.http.post(apiUrl, value, options)
    .map((res : Response) => res.json())
    .subscribe(
      response => {
        // Stop the spinner
        this.isWaiting = false;
        // Return the value from the server
        let errors = response;
        // If connected to server
        if(errors.errors){
          // If after filed the form server give back errors
          this.notify.show(`${errors.errors[Object.keys(errors.errors)[0]]}`, { position:'top', duration:'2000', type: 'error', sticky: true });
        }else{
          // Clear the existing localStorage
          localStorage.clear();
          // Make appear the box
          this.newsfeed = response.newsfeed;
          let token = response.token;
          this.localSt.store('token', token);
          let user = response.user;
          this.localSt.store('user', user);
          // Make appear the blur
          var divBlur = $("#blur");
          divBlur.addClass("blur nc-appearance-blur");
          $('body .nc-login-box #create').show();
        }

      },
      // Errors will call this callback instead:
      (err: HttpErrorResponse) => {
        // Stop the spinner
        this.isWaiting = false;
        if (err.error instanceof Error) {
          // A client-side or network error occurred. Handle it accordingly.
          console.log('An error occurred:', err.error.message);
          this.notify.show("Problem occurred, try later", { position:'top', duration:'2000', type: 'error', sticky: true });
        } else {
          // The backend returned an unsuccessful response code.
          // The response body may contain clues as to what went wrong,
          console.log(`Backend returned code ${err.status}, body was: ${err.statusText}`);
          this.notify.show("Problem occurred on the server, try again or contact the administrator", { position:'top', duration:'2000', type: 'error', sticky: true });
        }
      }
    );

  }

  /**
   * Passe to the main page
   */
  goToNewscrush() : void {
    // Make appear the blur
    var divBlur = $("#blur");
    divBlur.addClass("nc-appearance-blur-out");
    setTimeout("$('#blur').removeClass('blur nc-appearance-blur nc-appearance-blur-out');", 300);
    this.router.navigate(['/newscrush/', this.newsfeed]);
  }
}

import { TestBed, async } from '@angular/core/testing';
import { RouterModule, Router } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { LoginOrRegisterComponent } from './login/login_or_register.component';
import { LinkComponent } from './link.component';
import { AppComponent } from './app.component';
import { NamescapeService } from './services/namescape.service';
import { Http, HttpModule, BaseRequestOptions } from '@angular/http';
import { MockBackend, MockConnection } from '@angular/http/testing';
import { LocalStorageService } from 'ng2-webstorage';
import { RouterTestingModule } from '@angular/router/testing';
import { FormsModule } from '@angular/forms';


let mockRouter = {
  navigate: jasmine.createSpy('navigate')
}

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        LinkComponent,
        AppComponent,
        LoginOrRegisterComponent,
        LoginComponent,
      ],
      imports: [
        RouterTestingModule,
        FormsModule,
      ],
      providers: [
        {
          provide: Http, useFactory: (backend, options) => {
            return new Http(backend, options);
          },
          deps: [MockBackend, BaseRequestOptions]
        },
        {provide: Router, useValue: mockRouter},
        NamescapeService,
        MockBackend,
        BaseRequestOptions,
        LocalStorageService,
      ]
    }).compileComponents();
  }));

  it('router-outlet exist in app', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));

  it(`should have as title 'app works!'`, async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app.isConnected).toEqual(false);
  }));

  it('should render title in a h1 tag', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('router-outlet')).toBeTruthy(); // If that element exist in DOM
  }));
});

import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { Http, Response, Headers, RequestOptions, RequestMethod } from '@angular/http';
import { HttpErrorResponse } from '@angular/common/http';
import { NotificationService } from 'ng2-notify-popup';
import { DOCUMENT } from '@angular/platform-browser';
import { NamescapeService } from '../../services/namescape.service';
import { User } from '../../models/users.model';
import { Action } from '../../models/actions.model';
import { UserService } from '../../services/user.service';
import { GamificationService } from '../../services/gamification.service';
import { LocalStorageService } from 'ng2-webstorage';

import { environment } from '../../../environments/environment';

// Allow me to use jQuery
declare var jquery:any;   // not required
declare var $ :any;

@Component({
  selector: 'header',
  templateUrl: './header.component.html',
  providers: [NotificationService, UserService, GamificationService]
})

export class HeaderComponent implements OnInit {
  private headers: Headers = new Headers();
  private user: any = {};
  private gamification: any = {};
  private level : number = 0;
  private percentage : number = 0;
  private domain;

  constructor(
    private http: Http,
    private router: Router,
    private notify: NotificationService,
    private namescapeService: NamescapeService,
    private currentUser: User,
    private action: Action,
    private userService: UserService,
    private gamificationService: GamificationService,
    private localSt: LocalStorageService
  ) { }

  toggle() {
    $('.popup_profile').toggle();
  }

  ngOnInit(): void {
    // Initialise current user
    this.initialiseHeader();
    this.domain = environment.domain;
  }

  /**
   * Initialise header
   */
  initialiseHeader () {
    let promise = new Promise((resolve, reject) => {
      let apiUrl: string = environment.domain + 'api/user';
      this.http.post(apiUrl, {'token' : this.localSt.retrieve('token')})
      .toPromise()
      .then(
        res => { // Success
          this.user = res.json().result;
          this.level = this.gamificationService.getLevel(this.userService.getPoints(undefined, this.user)).level;
          this.percentage = this.userService.percentage(this.user);
          resolve();
        },
        msg => { // Error
          reject(msg);
        }
      );
    });
    return promise;
  }

  /**
   * Will logout the user
   */
  loginOut() {
    let apiUrl : string;
    apiUrl = environment.domain+'api/logout?token='+this.localSt.retrieve('token');
    return this.http.get(apiUrl)
      .subscribe(
        response => {
          if(response.statusText == 'OK') {
            this.router.navigate(['/login_or_register']);
            localStorage.clear();
          }
        },
        // Errors will call this callback instead:
        (err: HttpErrorResponse) => {
          if (err.error instanceof Error) {
            console.log('An error occurred:', err.error.message);
            this.notify.show("Problem occurred, try later", { position:'top', duration:'2000', type: 'error', sticky: true });
          } else {
            console.log(`Backend returned code ${err.status}, body was: ${err.statusText}`);
            this.notify.show("Problem occurred on the server, try again or contact the administrator", { position:'top', duration:'2000', type: 'error', sticky: true });
          }
        }
      );
  }
}

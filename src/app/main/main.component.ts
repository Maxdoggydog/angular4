import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Http, Response, Headers, RequestOptions, RequestMethod } from '@angular/http';
import { HttpErrorResponse } from '@angular/common/http';
import { NotificationService } from 'ng2-notify-popup';
import { DOCUMENT } from '@angular/platform-browser';
import { NamescapeService } from '../services/namescape.service';
import { User } from '../models/users.model';
import { Action } from '../models/actions.model';
import { LocalStorageService } from 'ng2-webstorage';
import { FrontService } from './../services/front.service';

import { environment } from './../../environments/environment';

// Allow me to use jQuery
declare var jquery:any;
declare var $ :any;

@Component({
  templateUrl: './main.component.html',
  providers: [NotificationService, User, Action, FrontService]
})

export class MainComponent implements OnInit {
  private headers: Headers = new Headers();
  data: any = {};
  private user: any = {};
  private gamification: any = {};
  constructor(
    private http: Http,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private notify: NotificationService,
    private namescapeService: NamescapeService,
    private currentUser: User,
    private action: Action,
    private localSt: LocalStorageService,
    private frontService: FrontService
  ) {

  }

  ngOnInit(): void {
    // Initialise current user
    let newsfeed_id : Number = parseInt(this.activatedRoute.snapshot.params['id'])
    if(!this.localSt.retrieve('user')){
      this.initialiseMain(newsfeed_id);
    }else{
      this.user = this.localSt.retrieve('user');
      this.checkNewsfeed (this.user, newsfeed_id);
    }
  }

  /**
   * Initialise Main
   */
  initialiseMain (newsfeed_id) {
    let apiUrl: string = environment.domain + 'api/user';
    return this.http.post(apiUrl, {'token' : this.localSt.retrieve('token')})
    .map((res : Response) => res.json())
    .subscribe(
      res => {// Success
        this.user = res.result;
        console.log('user main', this.user);
        this.localSt.store('user', this.user);
        this.checkNewsfeed (this.user, newsfeed_id);
      }
    );
  }

  /**
   * Check the newsfeed
   * And return to the good one
   */
  checkNewsfeed (user, newsfeed_id) {
    let apiUrl: string = environment.domain + 'api/newsfeedbelongstouser';
    this.http.post(apiUrl, {'token' : this.localSt.retrieve('token'), 'newsfeed': newsfeed_id})
    .map((res : Response) => res.json())
    .subscribe(
      data => {
        // Check if id of the newsfeed belongs to user
        // If not return to the good one
        if(!data.belongs){
          this.router.navigate(['/', user.newsfeed_ids[0]]);
        }
      }
    );
  }

  /**
   * Remove the blur effect
   */
  removeBlurEffect(){
    this.frontService.removeBlur();
  }

}

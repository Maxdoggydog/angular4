import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Http, Response, Headers, RequestOptions, RequestMethod } from '@angular/http';
import { HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { NamescapeService } from '../../services/namescape.service';
import { User } from '../../models/users.model';
import { Action } from '../../models/actions.model';
import { UserService } from '../../services/user.service';
import { FrontService } from '../../services/front.service';
import { LocalStorageService } from 'ng2-webstorage';

// Allow me to use jQuery
declare var jquery:any;   // not required
declare var $ :any;
import * as _ from 'underscore';
import 'rxjs/add/operator/toPromise';

import { environment } from '../../../environments/environment';

@Component({
  selector: 'panel-tags',
  templateUrl: './panel_tags.component.html',
  providers: [UserService, FrontService]
})

export class PanelTagsComponent implements OnInit {
  private user: any = {};
  private newsfeed_id;
  private team_id;
  private tags : Array<any>;
  private remaining;
  private NC_LIMITTAGS : number = 5;
  private load: boolean = true;

  constructor(
    private http: Http,
    private namescapeService: NamescapeService,
    private activatedRoute: ActivatedRoute,
    private userService: UserService,
    private frontService: FrontService,
    private localSt: LocalStorageService
  ){

  }

  ngOnInit(): void {
    // Initialise newsfeed and team
    this.activatedRoute.params.subscribe(params => {
      this.initialiseTags(params['id'], params['team']);
    });
  }

  /**
   * Return a tag if this one is actif
   */
  activeSearch (tag) {
    if (!this.activatedRoute.snapshot.params['tags'])
      return;
    let tags = this.activatedRoute.snapshot.params['tags'].split(",");
    return tags.indexOf(tag) !== -1;
  }

  /**
   * Return the occurence of one tag
   */
  occTags (tagName) {
    return this.frontService.occTags(tagName, this.newsfeed_id, this.team_id);
  }

  /**
   * Initialise tags
   */
  initialiseTags (newsfeed_id, team_id) {
    let promise = new Promise((resolve, reject) => {
      let apiUrl: string = environment.domain + 'api/getnews';
      this.http.post(apiUrl, {'token' : this.localSt.retrieve('token'), 'newsfeed_id': newsfeed_id, 'team_id': team_id})
      .toPromise()
      .then(
        res => { // Success
          let news;
          news = res.json().result;
          this.tags = news.map((v) => (v.tags)).reduce((tagA, tagB) => (_.union(tagA, tagB)), []);
          this.remaining = this.tags.length - this.NC_LIMITTAGS;
          this.load = false;
          resolve();
        }
      );
    });
    return promise;
  }

}

import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Http, Response, Headers, RequestOptions, RequestMethod } from '@angular/http';
import { HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { NamescapeService } from '../../services/namescape.service';
import { UserService } from '../../services/user.service';
import { FrontService } from '../../services/front.service';
import { LocalStorageService } from 'ng2-webstorage';
import 'rxjs/add/operator/toPromise';

import { environment } from '../../../environments/environment';

// Allow me to use jQuery
declare var jquery:any;   // not required
declare var $ :any;


@Component({
  selector: 'panel-teammates',
  templateUrl: './panel_teammates.component.html',
  providers: [UserService, FrontService]
})

export class PanelTeammatesComponent implements OnInit {
  private user: any = {};
  private gamification: any = {};
  private newsfeed_id;
  private team_id;
  private teammates : Array<any>;
  private remaining;
  private NC_LIMITTEAMMATES : number = 3;
  private load: boolean = true;

  constructor(
    private http: Http,
    private namescapeService: NamescapeService,
    private activatedRoute: ActivatedRoute,
    private userService: UserService,
    private frontService: FrontService,
    private localSt: LocalStorageService
  ){

  }

  ngOnInit(): void {
    // Initialise newsfeed and team
    this.activatedRoute.params.subscribe(params => {
      this.initialiseTeammate(params['id'], params['team']);
    });
  }

  /**
   * Return the number of post of a user did
   */
  nbPosts (user_id) {
    return this.frontService.nbPosts(user_id);
  };

  /**
   * Initialise teammates
   */
  initialiseTeammate (newsfeed_id, team_id?) {
    let promise = new Promise((resolve, reject) => {
      let apiUrl: string = environment.domain + 'api/getusers';
      this.http.post(apiUrl, {'token' : this.localSt.retrieve('token'), 'newsfeed_id': newsfeed_id, 'team_id': team_id})
      .toPromise()
      .then(
        res => { // Success
          this.teammates = res.json().result;
          this.teammates.sort(function (user_a, user_b) {
            return this.userService.getPoints(undefined, user_b) - this.userService.getPoints(undefined, user_a);
          });
          this.remaining = this.teammates.length - this.NC_LIMITTEAMMATES;
          this.load = false;
          resolve();
        }
      );
    });
    return promise;
  }

}

import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Http, Response, Headers, RequestOptions, RequestMethod } from '@angular/http';
import { HttpErrorResponse } from '@angular/common/http';
import { NotificationService } from 'ng2-notify-popup';
import { DOCUMENT } from '@angular/platform-browser';
import { NamescapeService } from '../../services/namescape.service';
import { GamificationService } from '../../services/gamification.service';
import { UserService } from '../../services/user.service';
import { User } from '../../models/users.model';
import { Action } from '../../models/actions.model';
import { News } from '../../models/news.model';
import { FrontService } from '../../services/front.service';
import { LocalStorageService } from 'ng2-webstorage';

import { environment } from '../../../environments/environment';

@Component({
  selector: 'main-news',
  templateUrl: './main_news.component.html',
  providers: [NotificationService, User, Action, News, FrontService]
})

export class MainNewsComponent implements OnInit {
  private headers: Headers = new Headers();
  private users: Array<User>;
  private news;
  private currentUser: any = {};
  private gamification: any = {};
  private newsfeed_id;
  private team_id;
  private LIMITVIEWERS;
  private LIMITCOMMENTERS;
  private export_news;
  private domain;
  private load: boolean = true;

  constructor(
    private http: Http,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private notify: NotificationService,
    private namescapeService: NamescapeService,
    private gamificationService: GamificationService,
    private userService: UserService,
    private user: User,
    private action: Action,
    private frontService: FrontService,
    private localSt: LocalStorageService
  ) {

  }

  ngOnInit(): void {
    this.LIMITVIEWERS = 5;
    this.LIMITCOMMENTERS = 3;
    this.currentUser = this.localSt.retrieve('user');
    this.domain = environment.domain;
    this.news = [];
    this.activatedRoute.params.subscribe(params => {
      this.initialiseNews(params['id'], params['team']);
      this.newsfeed_id = params['id'];
    });
  }

  /**
   * Initialise all the parameter news
   * Return all the news with 'newsfeed_id' and 'team_id'
   */
  initialiseNews(newsfeed_id, team_id?) {
    let apiUrl: string = environment.domain + 'api/getnews';
    return this.http.post(apiUrl, {'token' : this.localSt.retrieve('token'), 'newsfeed_id': newsfeed_id, 'team_id': team_id})
    .map((res : Response) => res.json())
    .subscribe(
      res => {
        this.news = res.result;
        this.load = false;
      }
    );
  }

  /**
   * Return boolean
   * If user_id can edit or delete news
   */
  editDel(user_id) : boolean{
    return true;
  }

  /**
   * Return boolean
   * Check if the notification is in table
   */
  inNottification(notification_id) : boolean {
    return true;
  }

  /**
   * Check if news is in current user favories
   */
  newsIsFavorite (news_id) {
    if (this.currentUser && this.currentUser.favorites) {
      return this.currentUser.favorites.filter((news) => (news.news_id === news_id)).length > 0;
    } else {
      return false;
    }
  }

  /**
   * Return the number comment
   * For the news Id
   */
  totalCommentNews (news_id) {

  }

  /**
   * Count type of medal
   * For this user and type
   */
  countMedalType (type, user) {
    //return this.userService.countMedalType (type, user);
  }

  /**
   * Return random avatar
   */
  default_avatar (){
    return this.namescapeService.default_avatar();
  }

  /**
   * Percentage for user
   */
  percentages(user){
    // return this.userService.percentages(user);
  }

  /**
   * Level for user
   */
  level(user) {
    let points = 0;
    // return this.gamificationService.getLevel(points);
  }

  /**
   * Return the avg of the news
   */
  stars(rate) {
    return this.frontService.stars(rate);
  }

  /**
   * Show news in the reader
   */
  showReaderNews(news_id) {
    this.frontService.showReader(this.newsfeed_id, news_id);
    this.frontService.showReaderNews(this.newsfeed_id, news_id, this.currentUser.id);
  }
}

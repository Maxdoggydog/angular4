import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Http, Response, Headers, RequestOptions, RequestMethod } from '@angular/http';
import { HttpErrorResponse } from '@angular/common/http';
import { NotificationService } from 'ng2-notify-popup';
import { DOCUMENT } from '@angular/platform-browser';
import { NamescapeService } from '../../../services/namescape.service';
import { GamificationService } from '../../../services/gamification.service';
import { NewsService } from '../../../services/news.service';
import { UserService } from '../../../services/user.service';
import { User } from '../../../models/users.model';
import { Action } from '../../../models/actions.model';
import { News } from '../../../models/news.model';
import { FrontService } from '../../../services/front.service';
import { LocalStorageService } from 'ng2-webstorage';

import { environment } from '../../../../environments/environment';

@Component({
  selector: 'reader',
  templateUrl: './reader.component.html',
  providers: [NotificationService, User, Action, News, FrontService, NewsService]
})

export class ReaderComponent implements OnInit {
  private headers: Headers = new Headers();
  private users: Array<User>;
  private news;
  private currentUser: any = {};
  private gamification: any = {};
  private newsfeed_id;
  private team_id;
  private news_id;
  private domain;
  private load: boolean = true;


  constructor(
    private http: Http,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private notify: NotificationService,
    private namescapeService: NamescapeService,
    private gamificationService: GamificationService,
    private userService: UserService,
    private user: User,
    private action: Action,
    private frontService: FrontService,
    private localSt: LocalStorageService,
    private newsService: NewsService,
  ) {

  }

  ngOnInit(): void {
    this.news_id = this.localSt.retrieve('current_news_clicked');
    this.initialiseNews(this.news_id);
    this.currentUser = this.localSt.retrieve('user');
    this.newsStarsRating(this.news_id);
    this.activatedRoute.queryParams.subscribe(queryParams => {
      this.initialiseNews(queryParams['news']);
    });
    this.domain = environment.domain;
  }

  /**
   * Initialise all the parameter news
   * Return al the news with 'news_id'
   */
  initialiseNews(news_id) {
    if(!news_id) return;
    let apiUrl: string = environment.domain + 'api/onenews';
    return this.http.post(apiUrl, {'token' : this.localSt.retrieve('token'), 'news_id': news_id})
    .map((res : Response) => res.json())
    .subscribe(
      res => {
        this.load = false;
        return this.news = res.result;
      }
    );
  }

  /**
   * Check if the user has the news in favorite
   */
  newsIsFavorite(news_id) {
    this.frontService.newsIsFavorite(news_id, this.currentUser);
  }

  /**
   * Initialise stars rating
   */
  newsStarsRating (news_id) {
    var user_id = this.currentUser.id;
    this.frontService.reader().userRate(user_id, news_id);
  }

  /**
   *
   */
  checkedNews(user_id, news_id){
    this.frontService.checkedNews(user_id, news_id);
  }

  /**
   *
   */
  nl2br(val){
    return this.frontService.nl2br(val);
  }
}

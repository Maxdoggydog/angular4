import { Component, OnInit } from '@angular/core';
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';
import { Router, ActivatedRoute } from '@angular/router';
import { Http, Response, Headers, RequestOptions, RequestMethod } from '@angular/http';
import { HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { NamescapeService } from '../../services/namescape.service';
import { User } from '../../models/users.model';
import { Action } from '../../models/actions.model';
import { UserService } from '../../services/user.service';
import { FrontService } from '../../services/front.service';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { NotificationService } from 'ng2-notify-popup';
import { waterfall } from 'async-waterfall';
import { sanitizeHtml } from 'sanitize-html';
import { LocalStorageService } from 'ng2-webstorage';
import { environment } from '../../../environments/environment';

import * as async from 'async'

import 'rxjs/add/observable/of';
import 'rxjs/add/operator/filter';

// Allow me to use jQuery
declare var jquery:any;   // not required
declare var $ :any;

@Component({
  selector: 'main-url',
  templateUrl: './main_url.component.html',
  providers: [UserService, FrontService, NotificationService]
})

export class MainUrlComponent implements OnInit {
  private focus : boolean = false;
  private reset : boolean = false;
  private tagList : Array<any>;
  private teamTagList : Array<any>;
  private tagListSuggestion : Array<any>;
  private teamTagListSuggestion : Array<any>;
  private user : User;
  private NC_DRAG_DROP_POST;
  private headers: Headers = new Headers();
  private newsfeed_id;
  private team_id;
  private url;

  constructor(
    private http: Http,
    private notify: NotificationService,
    private namescapeService: NamescapeService,
    private activatedRoute: ActivatedRoute,
    private userService: UserService,
    private frontService: FrontService,
    private localSt: LocalStorageService,
    private sanitizer: DomSanitizer
  ){

  }

  ngOnInit(): void {
    this.user = this.localSt.retrieve('user');
    this.newsfeed_id = parseInt(this.activatedRoute.snapshot.params['id']);
    this.team_id = this.user.team_private;
  }

  /**
   * If focus element
   * put focus to true
   */
  focusField () : void {
    this.focus = true;
  }

  /**
   * If focus out element
   * put back focus to false
   * If the field got a value doesn't put focus to false
   */
  focusOutField (event) : void {
    if(!event.target.value){
      this.focus = false;
      this.reset = false;
    }
  }

  /**
   * If change value
   * Show the button to erase the field
   */
  changeValue (event) : void {
    if(event.target.value == ''){
      this.reset = false;
    }
    this.reset = true;
    this.embedUrl();
  }

  /**
   * Erase the field
   * Hide the part to post a news
   */
  eraseField() : void {
    $('#embed_url').get(0).value = '';
    this.focus = false;
    this.reset = false;
    this.frontService.hideDragAndDropForm();
    this.NC_DRAG_DROP_POST = this.frontService.initPostNews();
  }

  /**
   * Action when add tag
   */
  onTagAdded(event) {
    console.log('ontagAdded', event);
  }

  /**
   * Action when add team tag
   */
  onTeamTagAdded(event) {
    console.log('onTeamTagAdded', event);
  }

  /**
   * Action when remove team tag
   */
  onTeamTagRemoved(event) {
    console.log('onTeamTagRemoved', event);
  }

  /**
   * Action when remove tag
   */
  onTagRemoved(event) {
    console.log('onTagRemoved', event);
  }

  /**
   * Highlight content if content exist
   */
  highlight() { // NEED TO POST FIRST

    this.localSt.store('NC_DRAG_DROP_POST', this.NC_DRAG_DROP_POST);
  }

  /**
   * Post news
   */
  post() {
    /**
     * If the news exit
     * Edit it
     */
    if($("#news_id").val().length){
      return;
    }

    /**
     * If the news doesn't exit
     * Creat it
     */
    $("#btn_highlight").attr("disabled", true);
    $("#btn_post").attr("disabled", true);
    if (!this.NC_DRAG_DROP_POST.embedStatus) {
      this.notify.show('An error occured when parsing the url', { position:'top', duration:'2000', type: 'error', sticky: true });
      return;
    }

    /**
     * Double Check the news
     * Can be post
     */
    let apiUrl: string = environment.domain+'api/checknewsinprivate';
    this.http.post(apiUrl, {'token' : this.localSt.retrieve('token'), 'newsfeed_id': this.newsfeed_id, 'team_id': this.team_id, 'url': this.url})
    .map((res : Response) => res.json())
    .subscribe(data => {
      if(!data.result){
        let apiUrl: string = environment.domain+'api/checknewsinnewsfeed';
        this.http.post(apiUrl, {'token' : this.localSt.retrieve('token'), 'newsfeed_id': this.newsfeed_id, 'team_id': this.team_id, 'url': this.url})
        .map((res : Response) => res.json())
        .subscribe(data => {
          if(!data.result){
            // Going to post the news
            let apiUrl: string = environment.domain+'api/checksource';
            this.http.post(apiUrl, {'token' : this.localSt.retrieve('token'), 'source': this.NC_DRAG_DROP_POST.source})
            .map((res : Response) => res.json())
            .subscribe(data => {
              // Got the source id
              let param = this.NC_DRAG_DROP_POST.data;
              param['source_id'] = data.result;
              param['tags'] = this.tagListSuggestion;
              param['tags_team'] = this.teamTagListSuggestion;
              param['media'] = this.NC_DRAG_DROP_POST.media;
              param['images'] = this.NC_DRAG_DROP_POST.images;
              param['description'] = $("#embed_description").val();

              let apiUrl: string = environment.domain+'api/post';
              this.http.post(apiUrl, {'token' : this.localSt.retrieve('token'), 'param': param})
              .map((res : Response) => res.json())
              .subscribe(
                data => {
                  if(data.result){
                    /**
                     * NEW activity
                     */
                    // ???

                    /***
                     * Reset the part to post
                     */
                    this.eraseField();

                    /**
                     * Point?
                     */
                    // ???

                    /**
                     * datalog
                     */
                    // ???

                    return this.notify.show('News posted', { position:'top', duration:'2000', type: 'success', sticky: true });
                  }
                },
                (err: HttpErrorResponse) => {
                  this.eraseField();
                  return this.notify.show('Internal error while creating news', { position:'top', duration:'2000', type: 'error', sticky: true });
                }
              );

            });

          }else{
            return this.notify.show('The news exist in the newscrush', { position:'top', duration:'2000', type: 'error', sticky: true });
          }
        });
      }else{
        return this.notify.show('The news exist in your private team', { position:'top', duration:'2000', type: 'error', sticky: true });
      }
    });
  }

  /**
   * Allow us to post news
   */
  embedUrl() {
    setTimeout(function () {
      let outputError = '';
      // $NC.initPostNews(); // Regarder à la fin si ça vaut le coup ou pas
      this.NC_DRAG_DROP_POST = this.frontService.initPostNews();
      console.log('NC_DRAG_DROP_POST ==>',this.NC_DRAG_DROP_POST)
      this.url = $('#embed_url').val();

      /**
       * TEST url
       */
      let reStartUrl = /^(http[s]?\:\/\/)/i;
      if (!this.url.match(reStartUrl)) {
        this.url = 'http://' + this.url;
      }

      var regUrl = /^(?:(?:https?|ftp):\/\/)(?:\S+(?::\S*)?@)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,}))\.?)(?::\d{2,5})?(?:[/?#]\S*)?$/i;
      if (!this.url.match(regUrl)) {
        this.notify.show('Malformed url', { position:'top', duration:'2000', type: 'error', sticky: true });
        return;
      } else {
        /**
         * TEST duplication url / user avec team Private
         */

        /**
         * Check if the news is in private team of the user
         */
        console.log('user',this.user);
        let apiUrl: string = environment.domain+'api/checknewsinprivate';
        this.http.post(apiUrl, {'token' : this.localSt.retrieve('token'), 'newsfeed_id': this.newsfeed_id, 'team_id': this.team_id, 'url': this.url})
        .map((res : Response) => res.json())
        .subscribe(data => {
          if(!data.result){
            /**
             * Check if the news is in newsfeed
             */
              let apiUrl: string = environment.domain+'api/checknewsinnewsfeed';
              this.http.post(apiUrl, {'token' : this.localSt.retrieve('token'), 'newsfeed_id': this.newsfeed_id, 'team_id': this.team_id, 'url': this.url})
              .map((res : Response) => res.json())
              .subscribe(data => {
                if(!data.result){
                  // Fermer la partie export quand elle existera
                  // ????
                  // Fermer la barre de recherche quand elle existera
                  // ????

                  /**
                   * init form
                   */
                  this.NC_DRAG_DROP_POST = this.frontService.initPostNews();

                  /**
                   * Show the part post news
                   * Put tagList and teamTagList to empty array
                   */
                  this.frontService.initialisePostPart(this.tagList, this.teamTagList);

                  /**
                   * Launch Spinner
                   */
                  $('#loading_create_news').show();

                  /**
                   * Load tags for autocompletion
                   */
                  this.tagListSuggestion = [];
                  this.teamTagListSuggestion = [];

                  /**
                   * AJAX EMBED.LY
                   */
                  // NEED TO ADD HEADER
                  this.headers.append('Content-Type', 'application/json');
                  this.headers.append('Access-Control-Allow-Origin', '*');
                  let myParams = new URLSearchParams();
                  let key = 'key='+environment.public.embed.key;
                  let options = new RequestOptions({ headers: this.headers});
                  this.http.get(environment.public.embed.url+key+'&url='+this.url+'&maxwidth=400', options)
                  .map((res : Response) => res.json())
                  .subscribe(
                    result => {
                      /**
                       * Stop Spinner
                       */
                      $('#loading_create_news').hide();
                      $('#sub_form_news_create').fadeIn(this.frontService.anim().subFormOpen);
                      $('#btn_highlight').attr('disabled', false);
                      $('#btn_post').attr('disabled', false);
                      let data = result;
                      this.NC_DRAG_DROP_POST.embed = data;

                      if (data.images.length) {
                        $('#embed_img').attr('src', data.images[0].url);
                      }

                      let clean_content = this.sanitizer.bypassSecurityTrustHtml(data.content);

                      if (data.content === null) data.content = '';
                      /**
                       * test if no content then no highlight
                       */
                      //console.log(data.content.length);
                      if (data.content.length == 0) {
                        $('#block_submit').hide();
                        $('#btn_post_direct').show();
                      } else {
                        $('#block_submit').show();
                        $('#btn_post_direct').hide();
                      }

                      /**
                       * RENDER
                       */
                      $('#render_title').html(data.title);
                      $('#embed_description').val(''/*data.description*/);
                      $('#embed_source_logo').attr('src', data.favicon_url);
                      $('#embed_content').html(clean_content['changingThisBreaksApplicationSecurity']);
                      $('#embed_content').attr('contenteditable', false);

                      /**
                       * SET session media + images
                       */
                      if (Object.keys(data.media).length) {
                        this.NC_DRAG_DROP_POST.media = data.media;
                      }
                      if (data.images.length) {
                        this.NC_DRAG_DROP_POST.images = data.images;
                      }

                      /**
                       * SET session postNews[data] + postNews[source]
                       */
                      this.NC_DRAG_DROP_POST.data = {
                        url: data.url,
                        title: data.title,
                        original_text: clean_content['changingThisBreaksApplicationSecurity'],
                        highlighted_text: '',
                        description: data.description,
                        published: data.published,
                        newsfeed_id: this.newsfeed_id
                      };

                      /**
                       *
                       * SOURCE infos via embed
                       */
                      let source = {
                        name: data.provider_name,
                        url: data.provider_url,
                        logo: data.favicon_url
                      };
                      this.NC_DRAG_DROP_POST.source = source;
                      $('#embed_source_name').html(data.provider_display);
                      $('#embed_source_url').html(data.provider_url);
                      $('#embed_source_logo').html(data.favicon_url);
                      this.NC_DRAG_DROP_POST.embedStatus = true;

                    },
                    (err: HttpErrorResponse) => {
                      this.NC_DRAG_DROP_POST.embedStatus = false;
                      $('#sub_form_news_create').fadeOut(this.frontService.anim().subFormOpen);
                      console.log("embed error");
                      return this.notify.show('Embed error', { position:'top', duration:'2000', type: 'error', sticky: true });
                    }
                  );
                }else{
                  return this.notify.show('The news exist in the newscrush', { position:'top', duration:'2000', type: 'error', sticky: true });
                }

              });
          }else{
            return this.notify.show('The news exist in your private team', { position:'top', duration:'2000', type: 'error', sticky: true });
          }

        });

      }

    }.bind(this), 100);
  };

}

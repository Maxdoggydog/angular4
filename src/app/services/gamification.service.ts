import { Injectable, Inject } from '@angular/core';
import { DOCUMENT } from '@angular/platform-browser';
import { Http, Response, Headers, RequestOptions, RequestMethod } from '@angular/http';
import { HttpErrorResponse } from '@angular/common/http';
import { NamescapeService } from './namescape.service';
import { LocalStorageService } from 'ng2-webstorage';

declare var jquery:any;
declare var $ :any;

import { environment } from '../../environments/environment';

@Injectable()
export class GamificationService {
  private headers: Headers = new Headers();
  private _data: any = {};
  constructor(@Inject(DOCUMENT) private document,
    private http: Http,
    private namescapeService: NamescapeService,
    private localSt: LocalStorageService
  )
  {

  }

  /**
   * Get the json for gamification
   */
  json () {
    // Get the call to get the json
    let apiUrl: string = environment.domain + 'api/getjson';
    return this.http.post(apiUrl, {'token' : this.localSt.retrieve('token')})
    .map((res : Response) => res.json())
    .map((data) => this._data = data.result.json)
  };

  /**
   * Get userLevel
   */
  getLevel(userPoints: number) {
    userPoints = userPoints ? userPoints : 0;
    let json;
    if(!this.localSt.retrieve('json'))
      this.json().subscribe(result => json = result);
    else
      json = this.localSt.retrieve('json');

    if (!json || !json || !json.levels) return;

    const gLevels = json.levels;

    for (let i = 0; i < gLevels.length; i++) {
      /**
       * test first element
       */
      if (i == 0 && userPoints < gLevels[i].points) {
        return gLevels[i];
      }
      /**
       * test last element
       */
      if (i == gLevels.length - 1) {
        return gLevels[i];
      }
      /**
       * other
       */
      if (i != 0 && i != gLevels.length - 1) {
        if (gLevels[i - 1].points <= userPoints && userPoints < gLevels[i].points) {
          return gLevels[i];
        }
      }
    }
  };

  /**
   * Get the good badge of the user
   */
  getBadge(action: string) {
    let json;
    if(!this.localSt.retrieve('json'))
      this.json().subscribe(result => json = result);
    else
      json = this.localSt.retrieve('json');
    const gamification = json;
    // console.log("this.localSt.retrieve('json')", this.localSt.retrieve('json'))
    // console.log("gamification", gamification)
    // console.log("gamification.badges", gamification.badges)
    const r = gamification.badges.filter((v, i, t) => {
      return (action == v.name)
    });
    return r.pop();
  };

  /**
   *
   */
  getLevelIndex(userLevel){
    return userLevel ? userLevel.level + 1 : 1;
  };

  /**
   * Get the level and return the point of the level
   */
  getLevelPoints(indexLevel) : number {
    let json;
    if(!this.localSt.retrieve('json'))
      this.json().subscribe(result => json = result);
    else
      json = this.localSt.retrieve('json');
    const gLevels = json.levels;
    let i = 0, points = 0;
    while (i < gLevels.length) {
      if (indexLevel == gLevels[i].level) {
        points = gLevels[i].points;
        break;
      }
      i++;
    }
    return points;
  };

}
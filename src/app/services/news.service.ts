import { Injectable, Inject } from '@angular/core';
import { DOCUMENT } from '@angular/platform-browser';
import { Http, Response, Headers, RequestOptions, RequestMethod } from '@angular/http';
import { HttpErrorResponse } from '@angular/common/http';
import { GamificationService } from './gamification.service';
import { User } from '../models/users.model';
import { NamescapeService } from './namescape.service';
import { LocalStorageService } from 'ng2-webstorage';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';
declare var jquery:any;
declare var $ :any;
import * as _ from 'underscore';

import { environment } from '../../environments/environment';

@Injectable()
export class NewsService {
  private headers: Headers = new Headers();
  private data: any = {};
  private users;
  public news: any;

  constructor(@Inject(DOCUMENT) private document,
    private http: Http,
    private gamificationService: GamificationService,
    private namescapeService: NamescapeService,
    private localSt: LocalStorageService
  ){

  }

  /**
   * Retunr the news depend of
   * Newsfeed or team id
   * TODO / CHECK IF IS USLESS
   */
  getNews(newsfeed_id, team_id?) {
    let apiUrl: string = environment.domain + 'api/getnews';
    this.http.post(apiUrl, {'token' : this.localSt.retrieve('token'), 'newsfeed_id': newsfeed_id, 'team_id': team_id})
    .map((res : Response) => res.json())
    .subscribe(
      data => {
        if(data.result){
          this.localSt.store('news', data.result);
        }
      }
    );
    return this.localSt.retrieve('news');
  }

  /**
   * Get news
   */
  getNewsR(news_id) : Observable<any> {
    let apiUrl: string = environment.domain + 'api/news/' + news_id;
    return this.http.get(apiUrl)
    .map((res : Response) => res.json())
    .map(news => {
      this.news = news;
      return this.news;
    })
    .catch((error: any) => {
      return Observable.throw((error ? error : 'Server error'))
    });
  }

}
import { Injectable, Inject } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { DOCUMENT } from '@angular/platform-browser';
import { Http, Response, Headers, RequestOptions, RequestMethod } from '@angular/http';
import { HttpErrorResponse } from '@angular/common/http';
import {LocalStorageService, SessionStorageService, LocalStorage, SessionStorage} from 'ng2-webstorage';
import { NamescapeService } from './namescape.service';
import { UserService } from './user.service';
import { NewsService } from './news.service';
import 'rxjs/add/operator/map';
declare let jquery:any;
declare let $ :any;
import * as _ from 'underscore';

import { environment } from '../../environments/environment';

@Injectable()
export class FrontService {
  constructor(@Inject(DOCUMENT) private document,
    private http: Http,
    private router: Router,
    private localSt: LocalStorageService,
    private namescapeService: NamescapeService,
    private userService: UserService,
    private newsService: NewsService,
    private activatedRoute: ActivatedRoute,
  )
  {

  }

  /**
   * Return start
   * For the rate part
   */
  stars (rate, options?) {
    let out = "";
    let val = Math.round(rate);
    val = (!Number.isInteger(rate)) ? val - 1 : val;
    for (let i = 0; i < val; i++) {
      out += "<i class='fa fa-star stars rate-star'></i>";
      if (!Number.isInteger(rate) && i == (val - 1)) {
        out += "<i class='fa fa-star-half stars rate-star'></i><i class='fa fa-star-o fa-flip-horizontal stars rate-star'></i>";
        break;
      }
    }
    let residual = 5 - rate;
    residual = (!Number.isInteger(rate)) ? residual - 1 : residual;
    for (let i = 0; i < residual; i++) {
      out += "<i class='fa fa-star-o stars rate-star'></i>";
    }

    return '<span class="nc-block-stars">' + out + '</span>';
  };

  /**
   * Remove blur effect
   */
  removeBlur() {
    let divBlur = $("#blur");
    divBlur.addClass("nc-appearance-blur-out");
    setTimeout("$('#blur').removeClass('blur nc-appearance-blur nc-appearance-blur-out');", this.anim().readerClose);
    if (this.localSt.retrieve('popupBadge')) {
      this.localSt.store('popupBadge', false);
    }
    if (this.localSt.retrieve('popupLevel')) {
      this.localSt.store('popupLevel', false);
    }
    // For reader
    this.removeReader();
  };

  /**
   * Open the reader
   */
  showBlur (overReader?) {
    let divBlur = $("#blur");
    divBlur.addClass("blur nc-appearance-blur");
    if (overReader) {
      divBlur.css("z-index", 100 + 10);
    }
  };

  showReader (newsfeed_id, news_id) {
    this.showBlur();
    let reader = $("#reader");
    reader.removeClass("nc-invisible nc-slide-reader-right");
    reader.show();

    this.router.navigate(['/'+newsfeed_id], { queryParams: { news:  news_id} });

    /**
     *
     * re-init blur from popup Gamification
     */
    let divBlur = $("#blur");
    divBlur.css("z-index", 100);

    $('#reader-content').removeClass('nc-invisible');

    if (this.isChrome()) {
      $('#hide_reader').removeClass('nc-invisible');
    }
    let readerContent = $("#parent_reader");
    readerContent.show();
    readerContent.addClass("nc-slide-reader-right");
    setTimeout("$('#reader').removeClass('nc-slide-reader-right');$('#parent_reader').removeClass('nc-slide-reader-right');$('#hide_reader').removeClass('nc-invisible');", this.anim().readerOpen);
    $("body").addClass("no-scroll");
  };

  /**
   * Display the reader
   * With the news
   */
  showReaderNews (newsfeed_id, news_id, user_id) {
    if (news_id) {
      this.localSt.store('current_news_clicked', news_id); // get actual news id for the reader

      if ($("#tuto_" + news_id).length) {
        this.localSt.store('current_tuto_clicked', news_id);
        this.showReaderTuto();
      } else {
        this.showReader(newsfeed_id, news_id);
        // if ($frontStorage.activitiesAdd('views', newsId)) {
        //   // $NC.newsSetViews(newsId, user_id); || A faire ==> mettre les viewers dans news
        // }
      }
    }
  }

  /**
   * Show reader tuto
   */
  showReaderTuto() {
    this.showBlur();
    let reader = $("#reader_tuto");
    reader.removeClass("nc-invisible nc-slide-reader-right");

    $('#tuto-content').removeClass('nc-invisible');

    if (this.isChrome()) {
      $('#hide_reader').removeClass('nc-invisible');
    }
    let readerContent = $("#parent_reader");
    readerContent.show();
    readerContent.addClass("nc-slide-reader-right");
    setTimeout("$('#reader_tuto').removeClass('nc-slide-reader-right');$('#parent_reader').removeClass('nc-slide-reader-right');$('#hide_reader').removeClass('nc-invisible');", this.anim().readerOpen);
    $("body").addClass("no-scroll");
  };

  /**
   * Remove reader
   */
  removeReader() {
    let reader = $('#reader');
    let readerContent = $('#parent_reader');
    reader.addClass('nc-slide-reader-right-out nc-appearance-blur-out');
    readerContent.addClass('nc-slide-reader-right-out nc-appearance-blur-out');

    $('#reader-content').addClass('nc-invisible');
    $('#hide_reader').addClass('nc-invisible');
    setTimeout("$('#reader').removeClass('nc-slide-reader-right-out nc-appearance-blur-out');$('#parent_reader').removeClass('nc-slide-reader-right-out nc-appearance-blur-out');$('#parent_reader').hide();$('#reader').hide();$('body').removeClass('no-scroll');", this.anim().readerClose);
    this.reader().showComment();
    /* Remove $frontStorage.save() Check utilities */
  };

  /**
   * Remove tuto reader
   */
  removeReaderTuto () {
    let reader = $('#reader_tuto');
    let readerContent = $('#parent_reader');
    reader.addClass('nc-slide-reader-right-out nc-appearance-blur-out');
    readerContent.addClass('nc-slide-reader-right-out nc-appearance-blur-out');

    $('#tuto-content').addClass('nc-invisible');
    $('#hide_reader').addClass('nc-invisible');
    setTimeout("$('#reader_tuto').removeClass('nc-slide-reader-right-out nc-appearance-blur-out');$('#parent_reader').removeClass('nc-slide-reader-right-out nc-appearance-blur-out');$('#parent_reader').hide();$('#reader_tuto').hide();$('body').removeClass('no-scroll');", this.anim().readerClose);
    this.reader().showComment();
  };

  /**
   * Actions in reader
   */
  reader() {
    let self = this;
    return {
      /**
       * Rate the news
       */
      userRate(user_id, news_id) {
        this.resetStars();
        let rates = self.checkedNews(user_id, news_id);
        if (rates[0]) {
          let rate = rates[0].votes.find(function (v) {
            if (v.user == user_id) return v
          });
          setTimeout(function () {
            self.newsStarsRating(rate.rating);
          }, 100);
        }
      },
      /**
       * Reset rate
       */
      resetStars() {
        let $button = $("form.nc-rating button");
        for (let i = 0; i < $button.length; i++) {
          $($button[i]).removeClass("fa-star").addClass("fa-star-o");
        }
      },
      /**
       * Show comment
       */
      showComment() {
        $("#area-reply").hide();
        $("#reply").show();
      },
      /**
       * Show reply for comment
       */
      showReply() {
        $("#area-reply").show();
        $("#reply").hide();
      }
    }
  }

  /**
   *
   */
  newsStarsRating (value) {
    let $button = $("form.nc-rating button");
    for (let i = 0; i < $button.length; i++) {
      if (value == 0) {
        $($button[i]).removeClass("fa-star").addClass("fa-star-o");
      } else if ($($button[i]).val() <= value) {
        $($button[i]).removeClass("fa-star-o").addClass("fa-star");
      }
    }
  };

  /**
   * Check if the vote news exist
   */
  checkedNews (user_id, news_id) {
    return true;//News.find({_id: newsId, 'votes.user': {$in: [userId]}}).fetch();
  }

  /**
   * Check if is chrome
   */
  isChrome () {
    return window.hasOwnProperty('google');
  }

  /**
   * Check if the news is user favories
   */
  newsIsFavorite (news_id, user){
    if (user && user.favorites) {
      return user.favorites.filter((news) => (news.news_id === news_id)).length > 0;
    } else {
      return false;
    }
  }

  /**
   * Return time for animation
   */
  anim() {
    return {
      subFormOpen: 600,
      subFormClose: 600,
      readerOpen: 300,
      readerClose: 300,
    }
  }

  /**
   * Get the query 'filter_team'
   */
  getQueryFilterTeam () {
    let team_id = this.activatedRoute.snapshot.params['team'];
    return team_id;
  }


  /**
   * Get the newsfeed id
   */
  getNewsfeedId () : number {
    let newsfeed_id = parseInt(this.activatedRoute.snapshot.params['id']);
    return newsfeed_id;
  }

  /**
   * Return teams details
   */
  teams () {
    return this.localSt.retrieve('newsfeed').teamsDetails;
  }

  /**
   * Return users front team
   * Or every user from newsfeed
   * TODO : CHECK IF USELESS
   */
  teammates (newsfeed_id, team_id, user) {
    if (user.team_private == team_id) return;

    let allTeammates = this.userService.getUsers(newsfeed_id, team_id) || [];
    allTeammates.sort(function (user_a, user_b) {
      return this.userService.getPoints(undefined, user_b) - this.userService.getPoints(undefined, user_a);
    });
    return allTeammates;
  }

  /**
   * Retunr the number of post of one user
   */
  nbPosts (user_id) : number {
    let cpt : number = 0;

    return cpt;
  }

  /**
   * Return all the tags of one newscrush
   * Depend of team id
   * TODO / CHECK IF IS USELESS
   */
  tags (newsfeed_id, team_id) {
    let news = this.newsService.getNews(newsfeed_id, team_id);
    return news.map((v) => (v.tags)).reduce((tagA, tagB) => (_.union(tagA, tagB)), []);
  }

  /**
   * Return the occurence of one tag
   */
  occTags (tagName, newsfeed_id, team_id) {
    let cpt = 0;
    let news = this.newsService.getNews(newsfeed_id, team_id);
    news.forEach(function(n){
      if(n.tags.indexOf(tagName) !== -1){
        cpt += 1;
      }
    });
    return cpt;
  }

  /**
   * Initialise param post to post new post
   */
  initPostNews () : Object {
    return {
      media: {},
      published: {},
      images: [],
      tags: [],
      tags_team: [],
      source: {},
      data: {},
      embed: {},
      embedStatus: false,
    };
  }

  /**
   * Show the part post news
   */
  initialisePostPart (tagList, teamTagList) : void {
    tagList = [];
    teamTagList = [];
    $('#render_title').html('');
    $('#news_id').val('');
    $('#embed_description').html('');
    $('#btn_highlight').attr('disabled', true);
    $('#btn_post').attr('disabled', true);
    $('#sub_form_hl').hide();
    $('#block_submit').hide();
    $('#btn_post_direct').hide();
    $('#btn_close_dragdrop').show();
    $('#loading_create_news').hide();
    $('#sub_form_news_create').hide();
  }

  /**
   * Hide part for post news
   */
  hideDragAndDropForm() : void {
    // NC_DRAG_DROP_FORM = 'off'; // autorise export ==> a virer
    // $('.bootstrap-tagsinput input').val(''); // virer tous les tags
    $('#embed_url').val('');
    $('#news_id').val('');
    $('#render_title').html('');
    $('#embed_description').html('');
    $('#btn_highlight').attr('disabled', true);
    $('#btn_post').attr('disabled', true);
    $('#sub_form_hl').hide();
    $('#loading_create_news').hide();
    $('#sub_form_news_create').fadeOut(this.anim().subFormClose);
    $('#btn_close_dragdrop').hide();
    $('#block_submit').hide();
    $('#btn_post_direct').hide();
  };

  /**
   *
   */
  nl2br (val) {
    return val.replace(/\n/g, '<br>');
  }
}
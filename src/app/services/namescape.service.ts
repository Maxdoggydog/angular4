import { Injectable, Inject } from '@angular/core';
import { DOCUMENT } from '@angular/platform-browser';
import { Http, Response, Headers, RequestOptions, RequestMethod } from '@angular/http';
import { HttpErrorResponse } from '@angular/common/http';
import {LocalStorageService, SessionStorageService, LocalStorage, SessionStorage} from 'ng2-webstorage';
declare var jquery:any;
declare var $ :any;
import 'rxjs/add/operator/map';

import { environment } from '../../environments/environment';

@Injectable()
export class NamescapeService {
  private headers: Headers = new Headers();
  data: any = {};
  constructor(@Inject(DOCUMENT) private document, private http: Http, private localSt: LocalStorageService){

  }

  /**
   * Return error type
   */
  errorRequest (err) : void {
    if (err.error instanceof Error) {
      console.log(`An error occurred client side : ${err.error.message}`);
    } else {
      console.log(`Backend returned code ${err.status}, body was: ${err.statusText}`);
    }
  }

  /**
   * Check if user connected
   * TODO : USELESS
   */
  isConnected () : Boolean {
    if(!this.localSt.retrieve('token')) return false;

    let apiUrl: string;
    apiUrl = environment.domain + 'api/user';

    this.http.post(apiUrl, {'token' : this.localSt.retrieve('token')})
      .map((res : Response) => res.json())
      .subscribe(
      data => {
        if(data.result == false){
          this.localSt.store('isConnected', false);
        }else{
          this.localSt.store('isConnected', true);
        }
      },
      (err: HttpErrorResponse) => {
        this.errorRequest(err);
        this.localSt.store('isConnected', false);
      }
    )
    return this.localSt.retrieve('isConnected');
  }

  /**
   * Get user
   * TODO : USELESS
   */
  getUser () : Object {
    let apiUrl: string = environment.domain + 'api/user';
    this.http.post(apiUrl, {'token' : this.localSt.retrieve('token')})
    .map((res : Response) => res.json())
    .subscribe(
      data => {
        this.localSt.store('user', data.result);
      },
      (err: HttpErrorResponse) => {
        this.errorRequest(err)
      }
    );
    return this.localSt.retrieve('user');
  }

  /**
   * Get user gamification
   */
  getGamification () : Object {
    let apiUrl: string = environment.domain + 'api/getgamification';
    this.http.post(apiUrl, {'token' : this.localSt.retrieve('token')})
    .map((res : Response) => res.json())
    .subscribe(
      data => {
        this.localSt.store('gamification', data.result);
      },
      (err: HttpErrorResponse) => {
        this.errorRequest(err)
      }
    );
    return this.localSt.retrieve('gamification');
  }

  /**
   * Get user newsfeed
   */
  getUserNewsfeed () : String {
    let apiUrl: string;
    apiUrl = environment.domain + 'api/getnewsfeeduser';
    this.http.post(apiUrl, {'token' : this.localSt.retrieve('token')})
    .map((res : Response) => res.json())
    .subscribe(
      data => {
        this.localSt.store('newsfeed', data.newsfeed);

      },
      (err: HttpErrorResponse) => {
        this.errorRequest(err)
      }
    );
    return this.localSt.retrieve('newsfeed');
  }

  /**
   * Check if the newsfeed belong to user
   * TODO : USELESS
   */
  newsfeedBelongsToUser (id: Number) : Boolean {
    let apiUrl: string;
    apiUrl = environment.domain + 'api/newsfeedbelongstouser';
    this.http.post(apiUrl, {'token' : this.localSt.retrieve('token'), 'newsfeed': id})
    .map((res : Response) => res.json())
    .subscribe(
      data => {
        this.localSt.store('belongs', data.belongs);
      },
      (err: HttpErrorResponse) => {
        this.errorRequest(err);
      }
    );
    return this.localSt.retrieve('belongs');
  }

  /**
   * Return random int
   */
  getRandomInt (min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
  }

  /**
   * Return random avatar
   */
  default_avatar (http?) {
    let random = this.getRandomInt(1, 9);
    if (http) {
      return "icons/avatars/avatars_" + random + ".png";
    }
    return "/icons/avatars/avatars_" + random + ".png";
  }
}
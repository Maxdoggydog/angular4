import { Injectable, Inject } from '@angular/core';
import { DOCUMENT } from '@angular/platform-browser';
import { Http, Response, Headers, RequestOptions, RequestMethod } from '@angular/http';
import { HttpErrorResponse } from '@angular/common/http';
import { GamificationService } from './gamification.service';
import { User } from '../models/users.model';
import { NamescapeService } from './namescape.service';
import { LocalStorageService } from 'ng2-webstorage';
import 'rxjs/add/operator/map';
declare var jquery:any;
declare var $ :any;
import * as _ from 'underscore';

import { environment } from '../../environments/environment';

@Injectable()
export class UserService {
  private headers: Headers = new Headers();
  private data: any = {};
  private users;

  constructor(@Inject(DOCUMENT) private document,
    private http: Http,
    private gamificationService: GamificationService,
    private namescapeService: NamescapeService,
    private localSt: LocalStorageService
  ){

  }

  /**
   * Get all user
   * Depend of the newsfeed or team
   */
  getUsers(newsfeed_id, team_id?) {
    let apiUrl: string = environment.domain + 'api/getusers';
    this.http.post(apiUrl, {'token' : this.localSt.retrieve('token'), 'newsfeed_id': newsfeed_id, 'team_id': team_id})
    .map((res : Response) => res.json())
    .subscribe(
      data => {
        if(data.result){
          this.localSt.store('users', data.result);
        }
      }
    );
    return this.localSt.retrieve('users');
  }

  /**
   * Get user points
   */
  getPoints(badge: string, user: User) : number {
    let res = this.resolvePoints(badge, user.gamification.badges);
    return res;
  }

  /**
   *
   */
  resolvePoints(badge: string, jsonBadges: Object) : number {
    if (badge == undefined) {
      let points : number = 0;
      const keys = _.keys(jsonBadges);
      keys.forEach((b) => {
        points += this.getPointsAction(b, jsonBadges);
      });
      return points;
    } else {
      /**
       * expert  no points
       */
      if (badge == "expert") {
        return 0;
      } else {
        return this.getPointsAction(badge, jsonBadges);
      }
    }
  }

  /**
   *
   */
  getPointsAction(badge: string, jsonBadges: Object) : number {
    let points : number = 0;
    if (jsonBadges.hasOwnProperty(badge)) {
      const ssbadges = _.keys(jsonBadges[badge]);
      ssbadges.forEach((ssbadge) => {
        if (ssbadge != 'points') {
          const typePoints = (ssbadge == 'count') ? 'default' : ssbadge;
          points += jsonBadges[badge][ssbadge] * this.gamificationService.getBadge(badge).points[typePoints];
        }
      })
      return points;
    } else {
      return 0;
    }
  }

  /**
   * Get the count of one 'badge'
   */
  getCountBadge(badge: string, user) {
    if (badge == undefined) {
      return;
    }

    if (user.gamification.badges[badge].count) {
      return user.gamification.badges[badge].count;
    } else {
      return 0;
    }
  }

  /**
   * Get the percentage of the user
   */
  percentage(user: User) : number {
    let userLevel = this.gamificationService.getLevel(this.getPoints(undefined, user));
    let startPoints = this.gamificationService.getLevelPoints(userLevel.level - 1);
    let endPoints = this.gamificationService.getLevelPoints(userLevel.level);
    let rangeLevel = endPoints - startPoints;
    let points = this.getPoints(undefined, user);
    let percentages = (points - startPoints) * 100 / (rangeLevel);
    return $.isNumeric(Math.round(percentages)) ? Math.min(Math.round(percentages), 100) : 0;
  }

  /**
   * Check if it's a private team
   */
  teamIsPrivate(user, team_id){
    if (!team_id) return false;
    try {
      return user.team_private == team_id;
    } catch (e) {
      throw new Error('team-private');
    }
  }

}